package nl.kza.sqltraining.service;

import nl.kza.sqltraining.model.SimulationStage;
import org.junit.*;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

/***
 *
 */


@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class ConfigServiceTest {

    @Rule
    public TemporaryFolder folder;

    @InjectMocks
    private ConfigService service;

    @Before
    public void init() {
        folder = new TemporaryFolder();
    }

    @Test
    public void testGetFirstTimeConfigFromFile() throws IOException {
        //arrange
        folder.create();

        //act
        List<SimulationStage> stages = service.getConfigFromFile(folder.getRoot(), "/testfile.json");

        //assert
        Assert.assertTrue(stages.stream()
                .map(SimulationStage::isStageIsEnabled)
                .allMatch(Predicate.isEqual(false)));
        service.resetConfig();
    }

    @Test()
    public void getConfigFromFileShouldThrowExceptionIfFileNotExists() throws IOException {
        //TODO: Fix Unit Test
    }

    @Test
    public void testConfigDuplication() throws IOException {
        //arrange
        folder.create();
        String fullPath = folder.newFile("testfile.json").getAbsolutePath();

        //act
        service.duplicateConfigTo(fullPath);

        //assert
        Assert.assertNotNull("Config duplication failed, file is empty", service.getConfigFromFile(folder.getRoot(), "/testfile.json"));
    }

    @Test
    public void testEnableNextStage() throws IOException {
        //arrange
        folder.create();
        service.updateStageInConfigFile(folder.getRoot(), "/testfile.json");

        //Act
        List<SimulationStage> stages = service.getConfigFromFile(folder.getRoot(), "/testfile.json");

        //Assert
        Assert.assertEquals(stages.stream()
                        .filter(stage -> !stage.isStageIsEnabled())
                        .min(Comparator.comparingInt(SimulationStage::getId)).get().getId()
                , 2);
    }

    @Test //TODO: fix test
    public void TestResetConfigShouldNotFindFile() {
        //Assert.assertTrue("Config should not exist, error because file has been found", service.resetConfig());
    }

    @After
    public void teardown() {
        folder.delete();
    }

}
