package nl.kza.sqltraining.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.stream.IntStream;


@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class RandomUtilServiceTest {

    @InjectMocks
    private RandomUtilService service;

	@Before
	public void init() {

	}

	@Test
    public void getRandBetweenOneAndMaxNumber(){
	    //arrange
	    int maxNr = 10;
	    for(int i=0; i < 100; i++){
            //act
            int received = service.getRandomNumberBetweenOneAndMax(maxNr);

            //assert
            Assert.assertTrue(received > 0);
            Assert.assertTrue(received <= maxNr);
        }
    }

    @Test
    public void productCountPerOrderGenerator(){
        //arrange
        for(int i=0; i < 100; i++){
            //act
            int received = service.productCountPerOrderGenerator(100);

            //assert
            Assert.assertTrue("value is " + received + " instead of >= 1",received > 1);
            Assert.assertTrue("value is " + received + " instead of <= 1",received <= 100);
        }
    }

    @Test
    public void minuteDivisionGenerator(){
        //arrange&act
        int[] divisionresult = service.minuteDivisionGenerator(1000);

        //assert
        Assert.assertEquals(60,divisionresult.length);
        Assert.assertEquals(IntStream.of(divisionresult).sum(),1000);
    }


}
