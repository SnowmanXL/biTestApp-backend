package nl.kza.sqltraining.controller;

import nl.kza.sqltraining.component.StageSimComponent;
import nl.kza.sqltraining.service.BatchPersitanceService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class StageControllerTest {

    @InjectMocks
    StageController stageController;

    @Mock
    BatchPersitanceService batchPersitanceService;

    @Mock
    StageSimComponent stageSimComponent;

    @Before
    public void init(){

    }

    @Test
    public void testStartRunCall(){
        stageController.startRun();
        verify(stageSimComponent).startNewStage();
    }

    @Test
    public void testDatabaseResetCall(){
        stageController.resetDatabase();
        verify(batchPersitanceService).resetDatabase();
    }
}
