package nl.kza.sqltraining.controller;

import nl.kza.sqltraining.service.ConfigService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static nl.kza.sqltraining.service.ConfigService.runConfig;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class ConfigControllerTest {

    String filename = "test";

    @InjectMocks
    private ConfigController configController;

    @Mock
    ConfigService configService;

    @Before
    public void init() throws Exception
    {
       /* Field field = ConfigController.class.getDeclaredField("runConfig");
        field.setAccessible(true);
        field.set(configController, filename);*/
    }

    @Test
    public void testGetConfigCall(){
        configController.getConfig();
        verify(configService).getConfigFromFile(runConfig);
    }

    @Test
    public void testResetConfigCall(){
        configController.resetConfig();
        verify(configService).resetConfig();
    }

}
