package nl.kza.sqltraining.component;

import nl.kza.sqltraining.dal.ProductHistoryRepository;
import nl.kza.sqltraining.dal.ProductRepository;
import nl.kza.sqltraining.model.Product;
import nl.kza.sqltraining.model.SimulationStage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.atLeast;


@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class ProductPopularityRangeComponentTest {

    private SimulationStage stage = new SimulationStage();

    @InjectMocks
    private ProductPopularityRangeComponent productPopularityRangeComponent;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ProductHistoryRepository productHistoryRepository;

    @Before
    public void init(){
        //setup a config class
        stage.setCumulativeCustomerDiscount(true);
        stage.setCustomerDiscount(true);
        stage.setId(1);
        stage.setMaxProductsPerOrder(2);
        stage.setNumberOfProducts(1);
        stage.setSimulationDate("date");
        stage.setStageName("name");
        stage.setSelectRandomProducts(false);
        stage.setPriceRounding(true);
        stage.setStageIsEnabled(true);
    }

    @Test
    public void testCreateProdPopularityList(){
        //arrange & act
        Set<Product> prodset = new HashSet<>(productPopularityRangeComponent.createPopularityList(stage,true));

        //assert
        Assert.assertEquals(stage.getNumberOfProducts(),prodset.size());
    }

    @Test
    public void testInsertInHistoryTable(){
        //arrange & act
        productPopularityRangeComponent.createPopularityList(stage,true);

        //assert
        verify(productHistoryRepository,atLeast(1)).insertProductHistory(1,11.0,0,100,"date");
    }

    @Test
    public void testNullProductsIsNotAccepted(){
        //arrange
        boolean gotNullPointerException=false;

        //act
        try {
            productPopularityRangeComponent.createPopularityList(stage,false);
        } catch (NullPointerException e){
            gotNullPointerException = true;
        }

        //assert
        Assert.assertTrue(gotNullPointerException);
        verify(productRepository).findAll();
    }

    @Test
    public void testRandomProductSelect(){
        //arrange
        stage.setSelectRandomProducts(true);

        //act
        List<Product> products = productPopularityRangeComponent.createPopularityList(stage,true);

        //assert
        Assert.assertNotNull(products);
    }
}
