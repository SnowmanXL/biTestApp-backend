package nl.kza.sqltraining.component;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.lang.reflect.Field;

import static org.hamcrest.CoreMatchers.containsString;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class SystemPerformanceComponentTest {

    int cpuMockValue = 1000;

    @InjectMocks
    private SystemPerformanceComponent systemPerformanceComponent;

    @Before
    public void init() throws NoSuchFieldException, IllegalAccessException {
        Field field = SystemPerformanceComponent.class.getDeclaredField("cpuMax");
        field.setAccessible(true);
        field.set(systemPerformanceComponent, cpuMockValue);
    }

    @Test
    public void testGetPerformanceStringIsNotNull(){
        //arrange & act & assert
        Assert.assertNotNull(systemPerformanceComponent.getPerformanceMetricsAsString());
    }

    @Test
    public void testGetPerformanceContainsStringValues(){
        //arrange & act
        String perfString = systemPerformanceComponent.getPerformanceMetricsAsString();

        //assert
        Assert.assertThat(perfString,containsString("Diskload"));
        Assert.assertThat(perfString,containsString("Memory"));
        Assert.assertThat(perfString,containsString("Network"));
        Assert.assertThat(perfString,containsString("Cpu usage"));
    }

    @Test
    public void testMaxCustomerCountIsMaxPerfComponent(){
        //arrange & act
        int maxCount = systemPerformanceComponent.getMaxCustomerCountPerHour(1000000);

        //assert
        Assert.assertEquals(cpuMockValue,maxCount);
    }

    @Test
    public void testMaxCustomerCountIsCalculated(){
        //arrange & act
        int maxCount = systemPerformanceComponent.getMaxCustomerCountPerHour(100);

        //assert
        Assert.assertEquals(100,maxCount);
    }
}
