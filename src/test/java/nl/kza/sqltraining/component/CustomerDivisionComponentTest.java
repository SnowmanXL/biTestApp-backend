package nl.kza.sqltraining.component;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class CustomerDivisionComponentTest {

    @InjectMocks
    CustomerDivisionComponent customerDivisionComponent;

    @Mock
    SystemPerformanceComponent systemPerformanceComponent;

    @Before
    public void init(){

    }

    @Test
    public void testNumberOfEntriesShouldBe24(){
        //arrange & act & assert
        Assert.assertEquals(24,customerDivisionComponent.getCustomerCountPerHour(100000).size());
    }

    @Test
    public void testDivision(){
        //arrange & act
        customerDivisionComponent.getCustomerCountPerHour(24);
        verify(systemPerformanceComponent,times(24)).getMaxCustomerCountPerHour(0);

    }
}
