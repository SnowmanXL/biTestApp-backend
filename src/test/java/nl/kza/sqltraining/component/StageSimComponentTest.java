package nl.kza.sqltraining.component;

import nl.kza.sqltraining.dal.CustomerRepository;
import nl.kza.sqltraining.model.Customer;
import nl.kza.sqltraining.service.BatchPersitanceService;
import nl.kza.sqltraining.service.ConfigService;
import nl.kza.sqltraining.service.RandomUtilService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.NoSuchElementException;

import static nl.kza.sqltraining.service.ConfigService.runConfig;
import static org.mockito.Mockito.verify;


//TODO: Finish unit-test class
@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class StageSimComponentTest {

    @InjectMocks
    private StageSimComponent stageSimComponent;

    @Mock
    private ConfigService configService;
    @Mock
    private RandomUtilService randomUtilService;
    @Mock
    private CustomerDivisionComponent customerDivisionComponent;
    @Mock
    private CustomerRepository customerRepository;
    @Mock
    private ProductPopularityRangeComponent productPopularityRangeComponent;
    @Mock
    private BatchPersitanceService batchPersitanceService;

    private static List<Customer> customerList;


    @Before
    public void init(){

    }

    @Test
    public void testPrivateMethodFindStage() throws Exception {
        try {
            Whitebox.invokeMethod(stageSimComponent, "getNextStage");
        } catch (NoSuchElementException e){
           verify(configService).getConfigFromFile(runConfig);
        }
    }

}
