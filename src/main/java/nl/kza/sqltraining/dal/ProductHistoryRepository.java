package nl.kza.sqltraining.dal;

import nl.kza.sqltraining.model.ProductHistory;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ProductHistoryRepository extends CrudRepository<ProductHistory, Long> {

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO product_history (id, productprice,productdiscount,prodpopularity,date) values (:customerId,:productprice,:proddiscount,:prodpopularity,:pdate)",nativeQuery = true)
    int insertProductHistory(@Param("customerId") int customerId
            , @Param("productprice") double orderprice
            , @Param("proddiscount") int proddiscount
            , @Param("prodpopularity") int prodpopularity
            , @Param("pdate") String date) ;
}
