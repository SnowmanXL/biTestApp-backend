package nl.kza.sqltraining.dal;

import nl.kza.sqltraining.model.Customer;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

    @Transactional
    @Modifying
    @Query("UPDATE Customer c SET c.ordercount = :ordercount WHERE c.id = :customerId")
    int updateCustomerOrderCount(@Param("customerId") int customerId, @Param("ordercount") int ordercount);
}
