package nl.kza.sqltraining.model;

/**
 * Created by Wluijk on 4/7/2017.
 */
public class HourDivision {
    private int hour;
    private int minVal;
    private int maxVal;

    public HourDivision(int hour, int minVal, int maxVal) {
        this.hour = hour;
        this.minVal = minVal;
        this.maxVal = maxVal;
    }

    public int getHour() {
        return hour;
    }

    public int getMinVal() {
        return minVal;
    }

    public int getMaxVal() {
        return maxVal;
    }
}
