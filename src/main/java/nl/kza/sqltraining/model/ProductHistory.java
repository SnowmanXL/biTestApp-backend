package nl.kza.sqltraining.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "product_history")
public class ProductHistory {

    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "productprice")
    private double price;

    @Column(name = "date")
   private String date;

    @Column(name = "productdiscount")
    private int prodDiscount;

    @Column(name = "prodpopularity")
    private int prodPopularity;

    public ProductHistory(Product prod, String date){
        this.date = date;
        this.id = prod.getProdID();
        this.price = prod.getProdPrice();
        this.prodDiscount = prod.getProdDiscount();
        this.prodPopularity = prod.getProdPopularity();
    }

    public int getId() {
        return id;
    }

    public double getPrice() {
        return price;
    }

    public String getDate() {
        return date;
    }

    public int getProdDiscount() {
        return prodDiscount;
    }

    public int getProdPopularity() {
        return prodPopularity;
    }
}
