package nl.kza.sqltraining.model;

public class SimulationStage {
    private int id;
    private String stageName;
    private String simulationDate;
    private int numberOfProducts;
    private boolean selectRandomProducts;
    private boolean customerDiscount;
    private boolean cumulativeCustomerDiscount;
    private int totalCustomerCountPerDay;
    private boolean stageIsEnabled;
    private int maxProductsPerOrder;

    public boolean isPriceRounding() {
        return priceRounding;
    }

    public void setPriceRounding(boolean priceRounding) {
        this.priceRounding = priceRounding;
    }

    private boolean priceRounding;


    public int getMaxProductsPerOrder() {
        return maxProductsPerOrder;
    }

    public void setMaxProductsPerOrder(int maxProductsPerOrder) {
        this.maxProductsPerOrder = maxProductsPerOrder;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public String getSimulationDate() {
        return simulationDate;
    }

    public void setSimulationDate(String simulationDate) {
        this.simulationDate = simulationDate;
    }

    public int getNumberOfProducts() {
        return numberOfProducts;
    }

    public void setNumberOfProducts(int numberOfProducts) {
        this.numberOfProducts = numberOfProducts;
    }

    public boolean isSelectRandomProducts() {
        return selectRandomProducts;
    }

    public void setSelectRandomProducts(boolean selectRandomProducts) {
        selectRandomProducts = selectRandomProducts;
    }

    public boolean isCustomerDiscount() {
        return customerDiscount;
    }

    public void setCustomerDiscount(boolean customerDiscount) {
        this.customerDiscount = customerDiscount;
    }

    public boolean isCumulativeCustomerDiscount() {
        return cumulativeCustomerDiscount;
    }

    public void setCumulativeCustomerDiscount(boolean cumulativeCustomerDiscount) {
        this.cumulativeCustomerDiscount = cumulativeCustomerDiscount;
    }

    public int getTotalCustomerCountPerDay() {
        return totalCustomerCountPerDay;
    }

    public void setTotalCustomerCountPerDay(int totalCustomerCountPerDay) {
        this.totalCustomerCountPerDay = totalCustomerCountPerDay;
    }

    public boolean isStageIsEnabled() {
        return stageIsEnabled;
    }

    public void setStageIsEnabled(boolean stageIsEnabled) {
        this.stageIsEnabled = stageIsEnabled;
    }
}
