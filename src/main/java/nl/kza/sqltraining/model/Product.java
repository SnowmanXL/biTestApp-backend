package nl.kza.sqltraining.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class Product {


    @Column(name = "productname")
    private String prodName;

    @Column(name = "productprice")
    private double prodPrice;

    @Column(name = "productdiscount")
    private int prodDiscount;

    @Column(name = "prodpopularity")
    private int prodPopularity;

    @Id
    @Column(name = "id")
    private int prodID;


    public Product(String prodName, double prodPrice, int prodDiscount, int prodPopularity, int prodID) {
        this.prodName = prodName;
        this.prodPrice = prodPrice;
        this.prodDiscount = prodDiscount;
        this.prodPopularity = prodPopularity;
        this.prodID = prodID;
    }

    public Product(){
        this(null,0,0,0,0);
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public double getProdPrice() {
        return prodPrice;
    }

    public void setProdPrice(double prodPrice) {
        this.prodPrice = prodPrice;
    }

    public int getProdDiscount() {
        return prodDiscount;
    }

    public void setProdDiscount(int prodDiscount) {
        this.prodDiscount = prodDiscount;
    }

    public int getProdPopularity() {
        return prodPopularity;
    }

    public void setProdPopularity(int prodPopularity) {
        this.prodPopularity = prodPopularity;
    }

    public int getProdID() {
        return prodID;
    }

    public void setProdID(int prodID) {
        this.prodID = prodID;
    }
}
