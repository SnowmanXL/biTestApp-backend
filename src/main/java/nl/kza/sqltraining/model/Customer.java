package nl.kza.sqltraining.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @Column(name = "id")
    private int customerId;

    @Column(name = "ordercount")
    private int ordercount;

    public int getCustomerId() {
        return customerId;
    }

    public int getOrdercount() {
        return ordercount;
    }
}
