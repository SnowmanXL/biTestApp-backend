package nl.kza.sqltraining.component;

import com.google.common.collect.Lists;
import nl.kza.sqltraining.dal.CustomerRepository;
import nl.kza.sqltraining.model.Customer;
import nl.kza.sqltraining.model.Product;
import nl.kza.sqltraining.model.SimulationStage;
import nl.kza.sqltraining.service.BatchPersitanceService;
import nl.kza.sqltraining.service.ConfigService;
import nl.kza.sqltraining.service.RandomUtilService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import static nl.kza.sqltraining.service.ConfigService.runConfig;

@Component
public class StageSimComponent {

    private final ConfigService configService;
    private final
    RandomUtilService randomUtilService;
    private final
    CustomerDivisionComponent customerDivisionComponent;
    private final CustomerRepository customerRepository;
    private final ProductPopularityRangeComponent productPopularityRangeComponent;
    private static List<Customer> customerList;
    private final BatchPersitanceService batchPersitanceService;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public StageSimComponent(ConfigService configService, CustomerDivisionComponent customerDivisionComponent, RandomUtilService randomUtilService, CustomerRepository customerRepository, ProductPopularityRangeComponent productPopularityRangeComponent, BatchPersitanceService batchPersitanceService) {
        this.configService = configService;
        this.customerDivisionComponent = customerDivisionComponent;
        this.randomUtilService = randomUtilService;
        this.customerRepository = customerRepository;
        this.productPopularityRangeComponent = productPopularityRangeComponent;
        this.batchPersitanceService = batchPersitanceService;
    }

    private SimulationStage getNextStage() {
        return configService.getConfigFromFile(runConfig).stream()
                .filter(stage -> !stage.isStageIsEnabled())
                .min(Comparator.comparingInt(SimulationStage::getId))
                .get();
    }

    //TODO: Create batch insert
    public void startNewStage() {
        List<String> batchList = simulateDay(getNextStage());
        for (String string : batchList) {
            log.debug(string);
        }

        batchPersitanceService.doBatchOperation(batchList);
        configService.updateStageInConfigFile(runConfig);
    }

    private ArrayList<String> simulateDay(SimulationStage config) {
        //Prepare run variables
        List<Product> prodList = productPopularityRangeComponent.createPopularityList(config,false);
        List<Integer> hourDiv = customerDivisionComponent.getCustomerCountPerHour(config.getTotalCustomerCountPerDay());
        customerList = Lists.newArrayList(customerRepository.findAll());
        Collections.shuffle(customerList);
        ArrayList<String> InsertList = new ArrayList<>();

        int timestapSeconds = 0;
        for (int hour : hourDiv) {
            int[] custCountPerMin = randomUtilService.minuteDivisionGenerator(hour);
            for (int custCount : custCountPerMin) {
                for (int i = 0; i <= custCount; i++) {
                    int timestamp = randomUtilService.getRandomNumberBetweenOneAndMax(60);
                    HashMap<String, String> orderDetails = getOrderDetails(config, prodList);


                    InsertList.add("Insert into customerorder (sprint,day,hour,orderid,totalprice,customerid) values ('1','"
                            + config.getSimulationDate()
                            + "','" + String.valueOf(timestamp + timestapSeconds)
                            + "','" + orderDetails.get("productId")
                            + "','" + orderDetails.get("productPrice")
                            + "','" + orderDetails.get("customerid")+"')");

                    //TODO: Figure out what to do with the customer update functionality
                    //customerUpdate.add("update customer set ordercount = "+ customerordercount + " where id = " + orderDetails.get("customerid"));
                }
                timestapSeconds += 60;
            }
        }
        return InsertList;
    }

    private HashMap<String, String> getOrderDetails(SimulationStage config, List<Product> prodList) {
        //Set up variables
        HashMap<String, String> orderDetails = new HashMap<>();
        int nrOfProducts = randomUtilService.productCountPerOrderGenerator(config.getMaxProductsPerOrder());
        Customer customer = customerList.get(0);
        String[] prodinorder = new String[nrOfProducts];
        double prodprice = 0;

        //create products for order and calculate order price:
        for (int i = 0; i < nrOfProducts; i++) {
            Product prod = prodList.get(ThreadLocalRandom.current().nextInt(0, prodList.size()));
            prodinorder[i] = String.valueOf(prod.getProdID());
            double price;

            if (config.isPriceRounding()) {
                price = ((double) Math.round(prod.getProdPrice() * 100) / 100);
            } else {
                price = prod.getProdPrice();
            }

            if (config.isCustomerDiscount()) {
                prodprice += price * ((100 - (double) prod.getProdDiscount()) / 100);
            } else {
                prodprice += price;
            }
        }

        //create productID for order:
        StringBuilder prodid = new StringBuilder("P");
        for (String id : prodinorder) {
            prodid.append(".").append(id);
        }

        orderDetails.put("productId", prodid.toString());

        //Add option for discount
        if (config.isCumulativeCustomerDiscount()) {
            prodprice -= customer.getOrdercount() * 0.123;
        }
        orderDetails.put("productPrice", String.valueOf(prodprice));
        orderDetails.put("customerid", String.valueOf(customer.getCustomerId()));

        //TODO: Check if this is performance wise the best solution, as this commits after every single update. Perhaps rework into a batch script.
        //customerRepository.updateCustomerOrderCount(customer.getCustomerId(),customer.getOrdercount()+1);

        //prevent duplicate customer numbers, thus removing the customer from the list
        customerList.remove(0);

        return orderDetails;
    }

}
