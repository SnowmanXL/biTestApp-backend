package nl.kza.sqltraining.component;

import nl.kza.sqltraining.model.HourDivision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Component
public class CustomerDivisionComponent {

    private final SystemPerformanceComponent systemPerformanceComponent;

    @Autowired
    public CustomerDivisionComponent(SystemPerformanceComponent systemPerformanceComponent) {
        this.systemPerformanceComponent = systemPerformanceComponent;
    }


    List<Integer> getCustomerCountPerHour(int totalCustomerCount) {
        List<Integer> customerCountPerHour = new ArrayList<>();
        List<Integer> customerWeightList = generateCustomerWeightList(stubArrayData());

        int customerPerWeight = totalCustomerCount / customerWeightList.stream().mapToInt(i -> i).sum();

        for (int weight : customerWeightList) {
            customerCountPerHour.add(systemPerformanceComponent.getMaxCustomerCountPerHour(weight * customerPerWeight));
        }
        return customerCountPerHour;
    }

    private List<Integer> generateCustomerWeightList(List<HourDivision> weightPerHour) {
        List<Integer> customerWeight = new ArrayList<>();
        for (HourDivision div : weightPerHour) {
            customerWeight.add(ThreadLocalRandom.current().nextInt(div.getMinVal(), div.getMaxVal()));
        }
        return customerWeight;
    }


    //TODO: Make dynamic
    private List<HourDivision> stubArrayData() {
        List<HourDivision> customerWeightPerHour = new ArrayList<>();
        customerWeightPerHour.add(new HourDivision(1, 2, 5));
        customerWeightPerHour.add(new HourDivision(2, 1, 4));
        customerWeightPerHour.add(new HourDivision(3, 1, 3));
        customerWeightPerHour.add(new HourDivision(4, 3, 5));
        customerWeightPerHour.add(new HourDivision(5, 5, 6));
        customerWeightPerHour.add(new HourDivision(6, 8, 9));
        customerWeightPerHour.add(new HourDivision(7, 12, 15));
        customerWeightPerHour.add(new HourDivision(8, 13, 19));
        customerWeightPerHour.add(new HourDivision(9, 15, 16));
        customerWeightPerHour.add(new HourDivision(10, 20, 25));
        customerWeightPerHour.add(new HourDivision(11, 14, 27));
        customerWeightPerHour.add(new HourDivision(12, 21, 38));
        customerWeightPerHour.add(new HourDivision(13, 23, 35));
        customerWeightPerHour.add(new HourDivision(14, 22, 29));
        customerWeightPerHour.add(new HourDivision(15, 15, 26));
        customerWeightPerHour.add(new HourDivision(16, 23, 25));
        customerWeightPerHour.add(new HourDivision(17, 38, 56));
        customerWeightPerHour.add(new HourDivision(18, 56, 69));
        customerWeightPerHour.add(new HourDivision(19, 44, 67));
        customerWeightPerHour.add(new HourDivision(20, 31, 41));
        customerWeightPerHour.add(new HourDivision(21, 22, 27));
        customerWeightPerHour.add(new HourDivision(22, 11, 15));
        customerWeightPerHour.add(new HourDivision(23, 6, 9));
        customerWeightPerHour.add(new HourDivision(24, 2, 5));
        return customerWeightPerHour;
    }
}
