package nl.kza.sqltraining.component;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Wluijk on 4/8/2017.
 */
@Component
public class SystemPerformanceComponent {
    private int cpuMax;
    private int diskMax;
    private int memoryAvailable;
    private int networkUsageMax;
    private int actualCustomerCount;


    public SystemPerformanceComponent() {
        stubPerformanceMetrics();
    }

    private void stubPerformanceMetrics(){
        cpuMax              = 5000;
        diskMax             = 16000;
        memoryAvailable     = 3000;
        networkUsageMax     = 19000;
    }

    //TODO: Insert in performance Table
    public String getPerformanceMetricsAsString(){
        actualCustomerCount = actualCustomerCount * 100;
        int cpuLoad         = actualCustomerCount/cpuMax>=100 ? 100 : actualCustomerCount/cpuMax;
        int diskLoad        = actualCustomerCount/diskMax>=100 ? 100 : actualCustomerCount/diskMax;
        int memoryUsage     = (actualCustomerCount*0.7)/memoryAvailable>=100 ? 100 : (int) ((actualCustomerCount * 0.7) / memoryAvailable);
        int networkUsage    = actualCustomerCount/networkUsageMax>=100 ? 100 : actualCustomerCount/networkUsageMax;
        return "Cpu usage all cores = " + cpuLoad +"% | Diskload = " + diskLoad + "% | Memory Usage = " + memoryUsage + "% | Network Usage = " + networkUsage + "%";
    }

    public int getMaxCustomerCountPerHour(int actualCustomerCount){
        this.actualCustomerCount = actualCustomerCount;
        List<Integer> allMetrics = new ArrayList<>();

        allMetrics.add(actualCustomerCount/cpuMax                   >=1 ? cpuMax            : actualCustomerCount);
        allMetrics.add(actualCustomerCount/diskMax                  >=1 ? diskMax           : actualCustomerCount);
        allMetrics.add((actualCustomerCount*0.7)/memoryAvailable    >=1 ? memoryAvailable   : actualCustomerCount);
        allMetrics.add(actualCustomerCount/networkUsageMax          >=1 ? networkUsageMax   : actualCustomerCount);

        Collections.sort(allMetrics);
        return allMetrics.get(0);
    }

}
