package nl.kza.sqltraining.component;

import com.google.common.collect.Lists;
import nl.kza.sqltraining.dal.ProductHistoryRepository;
import nl.kza.sqltraining.dal.ProductRepository;
import nl.kza.sqltraining.model.Product;
import nl.kza.sqltraining.model.SimulationStage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class ProductPopularityRangeComponent {
    private final
    ProductRepository productRepository;

    private final
    ProductHistoryRepository productHistoryRepository;

    @Autowired
    public ProductPopularityRangeComponent(ProductRepository productRepository, ProductHistoryRepository productHistoryRepository) {
        this.productRepository = productRepository;
        this.productHistoryRepository = productHistoryRepository;
    }

    List<Product> createPopularityList(SimulationStage config, boolean isstub) {
        List<Product> productPopularityList = new ArrayList<>();
        for (Product prod : getAllProducts(config,isstub)) {
            for (int i = 0; i < prod.getProdPopularity() + prod.getProdDiscount(); i++) {
                productPopularityList.add(prod);
            }
            //Update product history table
            productHistoryRepository.insertProductHistory(prod.getProdID()
                    ,prod.getProdPrice()
                    ,prod.getProdDiscount()
                    ,prod.getProdPopularity()
                    ,config.getSimulationDate());
        }
        return productPopularityList;
    }

    private List<Product> getAllProducts(SimulationStage config, boolean isstub) {
        List<Product> allProducts;
        if(isstub){
            allProducts = initiateProductList();
        } else {
            allProducts = Lists.newArrayList(productRepository.findAll());
        }

        if (config.isSelectRandomProducts())
            Collections.shuffle(allProducts);
        //TODO: Make sure we don't get an out of bounds exception
        allProducts.subList(config.getNumberOfProducts(), allProducts.size()).clear();
        return allProducts;
    }

    //TODO: stublist, move to testing class
    private List<Product> initiateProductList() {
        List<Product> availableProducts = new ArrayList<>();
        availableProducts.add(new Product("Kaasplankje", 11, 0, 100, 1));
        availableProducts.add(new Product("Kroketje", 14.64, 0, 100, 2));
        availableProducts.add(new Product("Frietje", 1.696, 30, 100, 3));
        availableProducts.add(new Product("MelkBroodje", 1.80, 0, 100, 4));
        return availableProducts;
    }
}
