package nl.kza.sqltraining.controller;

import nl.kza.sqltraining.model.SimulationStage;
import nl.kza.sqltraining.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static nl.kza.sqltraining.service.ConfigService.runConfig;


@RestController
@RequestMapping(path = "/config")
public class ConfigController {

    private final ConfigService configService;


    @Autowired
    public ConfigController(ConfigService configService) {
        this.configService = configService;
    }


    @RequestMapping(path = "/get")
    public List<SimulationStage> getConfig() {
        return configService.getConfigFromFile(runConfig).stream()
                .filter(SimulationStage::isStageIsEnabled)
                .collect(Collectors.toList());
    }

    @RequestMapping(path = "/reset")
    public boolean resetConfig() {
        return configService.resetConfig();
    }

}
