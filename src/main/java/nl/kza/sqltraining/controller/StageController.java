package nl.kza.sqltraining.controller;


import nl.kza.sqltraining.component.StageSimComponent;
import nl.kza.sqltraining.service.BatchPersitanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/stage")
public class StageController {

    private final BatchPersitanceService batchPersitanceService;

    private final StageSimComponent component;

    @Autowired
    public StageController(BatchPersitanceService batchPersitanceService, StageSimComponent component) {
        this.batchPersitanceService = batchPersitanceService;
        this.component = component;
    }

    @RequestMapping(path = "/startnext")
    public boolean startRun(){
        component.startNewStage();
        return true;
    }

    @RequestMapping(path = "/reset")
    public boolean resetDatabase(){
        return batchPersitanceService.resetDatabase();
    }
}
