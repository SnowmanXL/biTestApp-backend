package nl.kza.sqltraining.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.kza.sqltraining.model.SimulationStage;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationHome;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/***
 *
 */
@Service
public class ConfigService {

    private Resource baseConfig = new ClassPathResource("./config/stageconfig.json");
    private static final File JARDIR = new ApplicationHome(ConfigService.class).getDir();

    private Logger log = LoggerFactory.getLogger(this.getClass());


    public final static String runConfig = "\\stageconfig_run.json";

    /***
     *
     * @param fullFilePath
     * @throws IOException
     */
    public void duplicateConfigTo(String fullFilePath) throws IOException {
        InputStream stream = baseConfig.getInputStream();
        if (stream == null) {
            throw new NullPointerException("Cannot get resource \"" + fullFilePath + "\" from Jar file.");
        }

        int readBytes;
        byte[] buffer = new byte[4096];
        OutputStream resStreamOut = new FileOutputStream(fullFilePath);
        while ((readBytes = stream.read(buffer)) > 0) {
            resStreamOut.write(buffer, 0, readBytes);
        }
        resStreamOut.close();
        stream.close();
    }

    File duplicateConfigFileIfItNotExist(String fullpath) throws IOException {
        File file = new File(fullpath);
        if (!file.exists() || file.length() == 0) {
            duplicateConfigTo(fullpath);
        }
        return new File(fullpath);
    }

    public List<SimulationStage> getConfigFromFile(String filename) {
        return getConfigFromFile(JARDIR, filename);
    }

    @SuppressWarnings("unchecked")
    public List<SimulationStage> getConfigFromFile(File dir, String filename) {
        String fullpath = dir + filename;
        try {
            return new ObjectMapper().readValue(duplicateConfigFileIfItNotExist(fullpath), new TypeReference<List<SimulationStage>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void updateStageInConfigFile(String filename) {
        updateStageInConfigFile(JARDIR, filename);
    }

    //TODO: What happens when all stages are enabled?
    @SuppressWarnings("unchecked")
    public void updateStageInConfigFile(File dir, String filename) {
        try {
            log.debug(String.valueOf(dir));
            File file = duplicateConfigFileIfItNotExist(dir + filename);
            log.info(file.getAbsolutePath());
            JSONArray baseArray = (JSONArray) new JSONParser()
                    .parse(new FileReader(file));

            //get first configstage that needs to be enabled
            int nextStageId = getConfigFromFile(dir, filename).stream()
                    .filter(stage -> !stage.isStageIsEnabled())
                    .min(Comparator.comparingInt(SimulationStage::getId))
                    .get()
                    .getId();

            //match configstage with json array and update array
            for (int i = 0; i < baseArray.size(); i++) {
                JSONObject jsonObject = new JSONObject(baseArray.get(i).toString());
                log.debug(String.valueOf(jsonObject));

                if (Objects.equals(jsonObject.getString("stageIsEnabled"), "false")
                        && Objects.equals(jsonObject.getString("id"), String.valueOf(nextStageId))) {
                    jsonObject.put("stageIsEnabled", true);
                    baseArray.remove(i);
                    baseArray.add(i, jsonObject);
                    break;
                }
            }
            //overwrite configfile with updated stage info
            try (FileWriter fileWriter = new FileWriter(file)) {
                fileWriter.write(baseArray.toString());
                log.info("Successfully updated json object to file.");
                fileWriter.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean resetConfig() {
        File filetodelete = new File(JARDIR + runConfig);
        if(filetodelete.exists()) {
            if (!new File(JARDIR + runConfig).delete()) {
                try {
                    PrintWriter pw = new PrintWriter(JARDIR + runConfig);
                    pw.close();
                    return true;
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            return true;
        }
        return true;
    }

    public File getRunConfigFile() {
        return new File(JARDIR + runConfig);
    }

}
