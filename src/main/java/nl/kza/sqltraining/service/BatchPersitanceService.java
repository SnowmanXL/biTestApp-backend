package nl.kza.sqltraining.service;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/***
 * This service class is used to insert for instance the orderlist into the database
 * it is a seperate class as the JPA dal layer does not offer this functionality
 * for future developent a more high level approach will be sought using the EntityManager
 */

@Service
public class BatchPersitanceService {

    @Value("${spring.datasource.url}")
    private String databaseUrl;

    @Value("${spring.datasource.username}")
    private String user;

    @Value("${spring.datasource.password}")
    private String password;

    private static Connection conn = null;
    private static Statement stmt = null;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    public void doBatchOperation(List<String> insertsList) {
        log.info("Execute batch operation");
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(databaseUrl, user, password);
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            final int batchSize = 100;
            int count = 0;
            int numberOfInserts = insertsList.size();

            for (int i = 0; i < numberOfInserts; i++) {
                stmt.addBatch(insertsList.get(i));
                if (++count % batchSize == 0) {
                    stmt.executeBatch();
                }
            }
            stmt.executeBatch();
            conn.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

    }

    /***
     * Reset script to bring the database back in start position
     */
    //TODO: Hard path will not work when code is packaged to jar!
    public boolean resetDatabase() {

        Resource reset = new ClassPathResource("./database/reset.sql");
        try {
            // Create MySql Connection
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(databaseUrl, user, password);

            ScriptRunner sr = new ScriptRunner(con);
            Reader reader = new InputStreamReader(reset.getInputStream());
            sr.runScript(reader);
            return true;
        } catch (Exception e) { //TODO: Make exception handling less generic
            log.error("Failed to Execute update script"
                    + " The error is " + e.getMessage());
            return false;
        }
    }
}
