package nl.kza.sqltraining.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadLocalRandom;

@Service
public class RandomUtilService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    public int[] minuteDivisionGenerator(int customerNum) {
        log.debug("Number of customers: "+customerNum);
        int minutes = 60;
        java.util.Random g = new java.util.Random();

        int vals[] = new int[minutes];
        customerNum -= minutes;

        //if <0 number, customer count is set to 0;
        customerNum = customerNum <=0 ? 1 : customerNum;
        for (int i = 0; i < minutes - 1; ++i) {
            vals[i] = g.nextInt(customerNum);
        }
        vals[minutes - 1] = customerNum;

        java.util.Arrays.sort(vals);
        for (int i = minutes - 1; i > 0; --i) {
            vals[i] -= vals[i - 1];
        }
        for (int i = 0; i < minutes; ++i) {
            ++vals[i];
        }
        return vals;
    }

    public int productCountPerOrderGenerator(int maxProductCountPerOrder) {
        return (ThreadLocalRandom.current().nextInt(1, maxProductCountPerOrder + 1)
                + ThreadLocalRandom.current().nextInt(1, maxProductCountPerOrder + 1))
                / 2;
    }

    public int getRandomNumberBetweenOneAndMax(int maxNumber){
        return ThreadLocalRandom.current().nextInt(1, maxNumber + 1);
    }

}


