# What is it?
This project contains an application to generate data in a MySQL database. The data can then be used for training purposes.
The data contains several 'problems' which can be managed using a configuration file. The data is generated batch wise and the generation is divided in stages. 
The application provides the possibility to configure the stages in order to increase the complexity of the generated data with each stage. 

# Prerequisites
* You have a MySQL database running with an user having the rights to create and update a database.
Provide the credentials and the connection details in the **application.properties** file.
* You have stages setup in the **resources/config/stageconfig.json** file

# To run the application:
1. Open commandline in the root of the project.
1. Build the application using gradle, command: gradle clean build
1. Navigate to build/libs/ and run: java -jar sqltraining-0.9.jar

# To use the application:
* Open the browser at localhost:8089. 
* Click 'start next stage', the database will then be populated with data
* Each time 'start next stage' is clicked a new batch with data is populated in the database
* To reset the progress, click 'reset'